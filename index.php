<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link href='https://api.tiles.mapbox.com/mapbox-gl-js/v0.53.1/mapbox-gl.css' rel='stylesheet' />
    <link rel="stylesheet" href="css/style.css">
    <script src='https://api.tiles.mapbox.com/mapbox-gl-js/v0.53.1/mapbox-gl.js'></script>

    <!--/Part 2/-->
    <!-- Geocoder plugin -->
    <script src='https://api.mapbox.com/mapbox-gl-js/plugins/mapbox-gl-geocoder/v2.0.1/mapbox-gl-geocoder.js'></script>
    <link rel='stylesheet' href='https://api.mapbox.com/mapbox-gl-js/plugins/mapbox-gl-geocoder/v2.0.1/mapbox-gl-geocoder.css' type='text/css' />
    <!-- Turf.js plugin -->
    <script src='https://npmcdn.com/@turf/turf/turf.min.js'></script>

</head>
<body>
<div class='sidebar'>
    <div class='heading'>
        <h1>Our locations</h1>
    </div>
    <div id='listings' class='listings'></div>
</div>
<div id='map' class='map pad2'></div>


<script>
    // Nao entedi qual e a sua finalidade
    if (!('remove' in Element.prototype)) {
        Element.prototype.remove = function() {
            if (this.parentNode) {
                this.parentNode.removeChild(this);
            }
        };
    }
    // Esta parte e a key, tem como finalidade autenticar o uso do script
    mapboxgl.accessToken = 'pk.eyJ1IjoiYW5kcmVnYW1hIiwiYSI6ImNqdDhtZWpzNzBhY2ozeXRoNzM0anV1enoifQ.6EhHaaJ8JqGkvP_UvnzEPw';

    //Finalidade de mostrar o MAP do MapBox
    var map = new mapboxgl.Map({
        container: 'map',
        // style Do mapa
        style: 'mapbox://styles/mapbox/dark-v10',
        // posiçoes iniciais
        center: [-77.034084, 38.909671],
        //zoom
        zoom: 14
    });

    //Json onde seria as informações
    var stores = {
        "type": "FeatureCollection",
        "features": [
            {
                "type": "Feature",
                "geometry": {
                    "type": "Point",
                    "coordinates": [
                        -77.034084142948,
                        38.909671288923
                    ]
                },
                "properties": {
                    "phoneFormatted": "(202) 234-7336",
                    "phone": "2022347336",
                    "address": "1471 P St NW",
                    "city": "Washington DC",
                    "country": "United States",
                    "crossStreet": "at 15th St NW",
                    "postalCode": "20005",
                    "state": "D.C."
                }
            },
            {
                "type": "Feature",
                "geometry": {
                    "type": "Point",
                    "coordinates": [
                        -77.049766,
                        38.900772
                    ]
                },
                "properties": {
                    "phoneFormatted": "(202) 507-8357",
                    "phone": "2025078357",
                    "address": "2221 I St NW",
                    "city": "Washington DC",
                    "country": "United States",
                    "crossStreet": "at 22nd St NW",
                    "postalCode": "20037",
                    "state": "D.C."
                }
            },
            {
                "type": "Feature",
                "geometry": {
                    "type": "Point",
                    "coordinates": [
                        -77.043929,
                        38.910525
                    ]
                },
                "properties": {
                    "phoneFormatted": "(202) 387-9338",
                    "phone": "2023879338",
                    "address": "1512 Connecticut Ave NW",
                    "city": "Washington DC",
                    "country": "United States",
                    "crossStreet": "at Dupont Circle",
                    "postalCode": "20036",
                    "state": "D.C."
                }
            },
            {
                "type": "Feature",
                "geometry": {
                    "type": "Point",
                    "coordinates": [
                        -77.0672,
                        38.90516896
                    ]
                },
                "properties": {
                    "phoneFormatted": "(202) 337-9338",
                    "phone": "2023379338",
                    "address": "3333 M St NW",
                    "city": "Washington DC",
                    "country": "United States",
                    "crossStreet": "at 34th St NW",
                    "postalCode": "20007",
                    "state": "D.C."
                }
            },
            {
                "type": "Feature",
                "geometry": {
                    "type": "Point",
                    "coordinates": [
                        -77.002583742142,
                        38.887041080933
                    ]
                },
                "properties": {
                    "phoneFormatted": "(202) 547-9338",
                    "phone": "2025479338",
                    "address": "221 Pennsylvania Ave SE",
                    "city": "Washington DC",
                    "country": "United States",
                    "crossStreet": "btwn 2nd & 3rd Sts. SE",
                    "postalCode": "20003",
                    "state": "D.C."
                }
            },
            {
                "type": "Feature",
                "geometry": {
                    "type": "Point",
                    "coordinates": [
                        -76.933492720127,
                        38.99225245786
                    ]
                },
                "properties": {
                    "address": "8204 Baltimore Ave",
                    "city": "College Park",
                    "country": "United States",
                    "postalCode": "20740",
                    "state": "MD"
                }
            },
            {
                "type": "Feature",
                "geometry": {
                    "type": "Point",
                    "coordinates": [
                        -77.097083330154,
                        38.980979
                    ]
                },
                "properties": {
                    "phoneFormatted": "(301) 654-7336",
                    "phone": "3016547336",
                    "address": "4831 Bethesda Ave",
                    "cc": "US",
                    "city": "Bethesda",
                    "country": "United States",
                    "postalCode": "20814",
                    "state": "MD"
                }
            },
            {
                "type": "Feature",
                "geometry": {
                    "type": "Point",
                    "coordinates": [
                        -77.359425054188,
                        38.958058116661
                    ]
                },
                "properties": {
                    "phoneFormatted": "(571) 203-0082",
                    "phone": "5712030082",
                    "address": "11935 Democracy Dr",
                    "city": "Reston",
                    "country": "United States",
                    "crossStreet": "btw Explorer & Library",
                    "postalCode": "20190",
                    "state": "VA"
                }
            },
            {
                "type": "Feature",
                "geometry": {
                    "type": "Point",
                    "coordinates": [
                        -77.10853099823,
                        38.880100922392
                    ]
                },
                "properties": {
                    "phoneFormatted": "(703) 522-2016",
                    "phone": "7035222016",
                    "address": "4075 Wilson Blvd",
                    "city": "Arlington",
                    "country": "United States",
                    "crossStreet": "at N Randolph St.",
                    "postalCode": "22203",
                    "state": "VA"
                }
            },
            {
                "type": "Feature",
                "geometry": {
                    "type": "Point",
                    "coordinates": [
                        -75.28784,
                        40.008008
                    ]
                },
                "properties": {
                    "phoneFormatted": "(610) 642-9400",
                    "phone": "6106429400",
                    "address": "68 Coulter Ave",
                    "city": "Ardmore",
                    "country": "United States",
                    "postalCode": "19003",
                    "state": "PA"
                }
            },
            {
                "type": "Feature",
                "geometry": {
                    "type": "Point",
                    "coordinates": [
                        -75.20121216774,
                        39.954030175164
                    ]
                },
                "properties": {
                    "phoneFormatted": "(215) 386-1365",
                    "phone": "2153861365",
                    "address": "3925 Walnut St",
                    "city": "Philadelphia",
                    "country": "United States",
                    "postalCode": "19104",
                    "state": "PA"
                }
            },
            {
                "type": "Feature",
                "geometry": {
                    "type": "Point",
                    "coordinates": [
                        -77.043959498405,
                        38.903883387232
                    ]
                },
                "properties": {
                    "phoneFormatted": "(202) 331-3355",
                    "phone": "2023313355",
                    "address": "1901 L St. NW",
                    "city": "Washington DC",
                    "country": "United States",
                    "crossStreet": "at 19th St",
                    "postalCode": "20036",
                    "state": "D.C."
                }
            }
        ]
    };

    //Ira fazer aparecer os Layers (incons) no mapa
    map.on('load', function (e) {
        // This is where your '.addLayer()' used to be, instead add only the source without styling a layer
        map.addSource("places", {
            "type": "geojson",
            "data": stores
        });
        // Faz aparecer as informações na lateral
        buildLocationList(stores);

        // Barra de pesquisa
        var geocoder = new MapboxGeocoder({
            accessToken: mapboxgl.accessToken,
            bbox: [-77.210763, 38.803367, -76.853675, 39.052643]
        });

        map.addControl(geocoder, 'top-left');

        //Marcação no map atraves de uma pesquisa
        map.addSource('single-point', {
            type: 'geojson',
            data: {
                type: 'FeatureCollection',
                features: [] // Notice that initially there are no features
            }
        });

        map.addLayer({
            id: 'point',
            source: 'single-point',
            type: 'circle',
            paint: {
                'circle-radius': 10,
                'circle-color': '#007cbf',
                'circle-stroke-width': 3,
                'circle-stroke-color': '#fff'
            }
        });

        //Finalidade de marca um ponto no mapa
        geocoder.on('result', function(ev) {
            var searchResult = ev.result.geometry;
            map.getSource('single-point').setData(searchResult);
            for (i=0; i < stores.features.length; i++) {
                var options = {units: 'kilometers'};

                Object.defineProperty(stores.features[i].properties, 'distance', {
                    value: turf.distance(searchResult, stores.features[i].geometry, options),
                    writable: true,
                    enumerable: true,
                    configurable: true
                });
            }
            stores.features.sort(function(a,b){
                if (a.properties.distance > b.properties.distance) {
                    return 1;
                }
                if (a.properties.distance < b.properties.distance) {
                    return -1;
                }
                // a must be equal to b
                return 0;
            });

            var listings = document.getElementById('listings');
            while (listings.firstChild) {
                listings.removeChild(listings.firstChild);
            }

            buildLocationList(stores);

            function sortLonLat(storeIdentifier) {
                var lats = [stores.features[storeIdentifier].geometry.coordinates[1], searchResult.coordinates[1]];
                var lons = [stores.features[storeIdentifier].geometry.coordinates[0], searchResult.coordinates[0]];

                var sortedLons = lons.sort(function(a, b) {
                    if (a > b) {
                        return 1;
                    }
                    if (a.distance < b.distance) {
                        return -1;
                    }
                    return 0;
                });
                var sortedLats = lats.sort(function(a, b) {
                    if (a > b) {
                        return 1;
                    }
                    if (a.distance < b.distance) {
                        return -1;
                    }
                    return 0;
                });

                map.fitBounds([
                    [sortedLons[0], sortedLats[0]],
                    [sortedLons[1], sortedLats[1]]
                ], {
                    padding: 100
                });
            }

            sortLonLat(0);
            createPopUp(stores.features[0]);
        });

    });

    stores.features.forEach(function(marker, i) {
        // Create an img element for the marker
        var el = document.createElement('div');
        el.id = "marker-" + i;
        el.className = 'marker';
        // Add markers to the map at all points
        new mapboxgl.Marker(el, {offset: [0, -23]})
            .setLngLat(marker.geometry.coordinates)
            .addTo(map);

        el.addEventListener('click', function(e){
            // 1. Fly to the point
            flyToStore(marker);

            // 2. Close all other popups and display popup for clicked store
            createPopUp(marker);

            // 3. Highlight listing in sidebar (and remove highlight for all other listings)
            var activeItem = document.getElementsByClassName('active');

            e.stopPropagation();
            if (activeItem[0]) {
                activeItem[0].classList.remove('active');
            }

            var listing = document.getElementById('listing-' + i);
            listing.classList.add('active');

        });
    });


    function flyToStore(currentFeature) {
        map.flyTo({
            center: currentFeature.geometry.coordinates,
            zoom: 15
        });
    }

    function createPopUp(currentFeature) {
        var popUps = document.getElementsByClassName('mapboxgl-popup');
        if (popUps[0]) popUps[0].remove();


        var popup = new mapboxgl.Popup({closeOnClick: false})
            .setLngLat(currentFeature.geometry.coordinates)
            .setHTML('<h3>Sweetgreen</h3>' +
                '<h4>' + currentFeature.properties.address + '</h4>')
            .addTo(map);
    }


    function buildLocationList(data) {
        for (i = 0; i < data.features.length; i++) {
            var currentFeature = data.features[i];
            var prop = currentFeature.properties;

            var listings = document.getElementById('listings');
            var listing = listings.appendChild(document.createElement('div'));
            listing.className = 'item';
            listing.id = "listing-" + i;

            var link = listing.appendChild(document.createElement('a'));
            link.href = '#';
            link.className = 'title';
            link.dataPosition = i;
            link.innerHTML = prop.address;

            var details = listing.appendChild(document.createElement('div'));
            details.innerHTML = prop.city;
            if (prop.phone) {
                details.innerHTML += ' &middot; ' + prop.phoneFormatted;
            }
            if (prop.distance) {
                var roundedDistance = Math.round(prop.distance*100)/100;
                details.innerHTML += '<p><strong>' + roundedDistance + ' kilometers</strong></p>';
            }



            link.addEventListener('click', function(e){
                // Update the currentFeature to the store associated with the clicked link
                var clickedListing = data.features[this.dataPosition];

                // 1. Fly to the point
                flyToStore(clickedListing);

                // 2. Close all other popups and display popup for clicked store
                createPopUp(clickedListing);

                // 3. Highlight listing in sidebar (and remove highlight for all other listings)
                var activeItem = document.getElementsByClassName('active');

                if (activeItem[0]) {
                    activeItem[0].classList.remove('active');
                }
                this.parentNode.classList.add('active');

            });
        }
    }
</script>
</body>
</html>